/* 
  	EDIT THIS FILE SO THAT IT CONTAINS PROCEDURES WHICH CAN BE IMPORTED INTO YOUR MYSQL SCHEMA
	You will need to have sufficent procedures to facilitate menu options 1 to 6
	Informaiton on the correct formation of procedures can be found here

	Note: do not use the key words "BEGIN" and "END" as they are not recognised when the .sql file is being imported. 

	Use the "DROP PROCEDURE" command to allow you to up date existing scripts with subsuequent imports. 
 	Take care if you rename procedures that you DROP the old procedures
*/

-- Option 1
DROP PROCEDURE IF EXISTS getEmployeeCount;
-- Option 2
DROP PROCEDURE IF EXISTS getAllEmployeeDetails;
-- Option 3
DROP PROCEDURE IF EXISTS getEmployeeByLastName;
-- Option 4
DROP PROCEDURE IF EXISTS insertEmployee;
-- Option 4.1
DROP PROCEDURE IF EXISTS insertSalary;
-- Option 4.2
DROP PROCEDURE IF EXISTS checkEmployeeExists;
-- Option 5
DROP PROCEDURE IF EXISTS getAllDetailsAndSalaries;
-- Option 6
DROP PROCEDURE IF EXISTS getEmployeeDetailsByDepartment;

-- Option 0 - no procedure required

-- Option 1 
CREATE PROCEDURE getEmployeeCount()
SELECT COUNT(emp_no) AS num FROM oop_employees;

-- Option 2 
CREATE PROCEDURE getAllEmployeeDetails()
SELECT * FROM oop_employees;

-- Option 3
CREATE PROCEDURE getEmployeeByLastName(IN lname VARCHAR(30))
SELECT * FROM oop_employees
WHERE last_name = lname;

-- OPTION 4 - Insert new row into oop_employees

CREATE PROCEDURE insertEmployee(IN emp_no INT, birth_date VARCHAR(10), first_name VARCHAR(30), last_name VARCHAR(30), 
								gender CHAR, hire_date VARCHAR(10))
INSERT INTO oop_employees 
VALUES (emp_no, birth_date, first_name, last_name, gender, hire_date);

-- OPTION 4.1 - Insert salary 

CREATE PROCEDURE insertSalary(IN emp_no INT, salary INT)
INSERT INTO oop_salaries 
VALUES (emp_no, salary);

-- Option 4.2 - Check if employee exists
 
CREATE PROCEDURE checkEmployeeExists(IN employee_no INT)
SELECT COUNT(*) FROM oop_employees WHERE emp_no = employee_no;

-- Option 5 - Get all details and salaries
CREATE PROCEDURE getAllDetailsAndSalaries()
SELECT employees.*, salaries.salary
FROM oop_employees employees
LEFT JOIN oop_salaries salaries ON employees.emp_no = salaries.emp_no;

-- Option 6 - to be defined by student, should involved more than one table

CREATE PROCEDURE getEmployeeDetailsByDepartment()
SELECT department_no.dept_no, department_name.dept_name, employees.*, salaries.salary
FROM oop_employees employees
INNER JOIN oop_salaries salaries ON employees.emp_no=salaries.emp_no
INNER JOIN oop_dept_emp department_no ON employees.emp_no=department_no.emp_no
INNER JOIN oop_departments department_name ON department_no.dept_no=department_name.dept_no
ORDER BY department_no.dept_no;