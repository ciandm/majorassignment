
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Scanner;
import util.DBConnect;

/**
 * FOLLOW THE COMMENTS CAREFULLY TO COMPLETE THIS FILE
 * 
 * A user menu facilitates interaction with a MySQL schema by calling methods
 * from this file which execute SQL procedures stored in a MYSQL schema.
 * Connection to this is specified by the user. Ensure that the correct name for
 * the style of connection (setUpForLocalMySQL or setUpForKnuthMySQL) you are
 * using is accessible (uncommented)
 * 
 * Common Errors - "The last packet sent successfully to the server was 0
 * milliseconds ago. The driver has not received any packets from the server." -
 * Your MySQL server is not running - There is no MySQL server running on the
 * port you have specified
 * 
 * @author Alex Cronin
 */
public class MajorAssignment {

	public static void main(String[] args) {
		System.out.println("MajorAssignment.java - starting main method\n");
		// Create a scanner to accept input from the user
		Scanner sc = new Scanner(System.in);
		// Create a DBConnect object and open a connection
		DBConnect dbc2 = new DBConnect();
		dbc2.setUpForKnuthMySQL("s2988638", "ittatent", "s2988638", "s2988638", "ittatent");
		dbc2.openConnection();
		// Below are two options ( a and b) for how to access your MySQL .
		// - Comment out the option you DO NOT want
		// - then fill in the appropriate values in the constructor
		// MySQL username: s2988638
		// MySQL password: ittatent
		// MySQL database: s2988638
		// SSH username: s2988638
		// SSH password: ittatent

		do {
			// Menu of the application
			System.out.println("\n===========================================================");
			System.out.println("USER MENU");
			System.out.println("Please select an option by typing the corresponding number.");
			System.out.println("0. Exit");
			System.out.println("1. Display the count of the employees");
			System.out.println("2. Display records of all employees");
			System.out.println("3. Search for an employee by last name");
			System.out.println("4. Add a new employee");
			System.out.println("5. Display employee details with salaries");
			System.out.println("6. Display employee details with salaries sorted by department number");
			System.out.println("===========================================================");
			System.out.print("option> ");

			// Initialise a variable of the correct type called option to store the value
			// entered by the user
			int option = sc.nextInt();

			/**
			 * When the user selects an option of 7 options are executed. Option 0 and 1
			 * specified. You must specify options 2 to 6. Each option will call a method
			 * below No method is required for option 0. Method for option 1 is specified
			 * below. You must specify methods of option 2 to 6.
			 */

			// 0. exit the program
			if (option == 0) {
				System.out.println("Exiting the program");
				break; // exit the while loop
			}

			// 1. displaying employee count
			else if (option == 1) {
				/**
				 * Output the result of the getEmployeeCount method which is implemented after
				 * the main method result is taken from getEmployeeCount which is returned
				 * through the SQL script
				 */
				int result = MajorAssignment.getEmployeeCount(dbc2.getConnection());
				// Displays the result of getEmployeeCount to user
				System.out.println("The employee count is: " + result);
				System.out.println(" ");
				// I want to make sure the displayed result wont be lost when menu comes up
				// again
				System.out.print("Press any key and hit enter to continue: ");
				// This is probably bad practice with the yellow error but I'm not sure how to
				// "use" the variable
				// to remove it
				String anyKey = sc.next();
			}

			// 2. displaying all employees
			else if (option == 2) {
				// Creates an Employee array using return value from getAllEmployeeDetails
				Employee[] result = MajorAssignment.getAllEmployeeDetails(dbc2.getConnection());
				// Initialise an int named i
				int i;
				// i starts at 0 and while i is less than the length of the resulting array
				// from getAllEmployeeDetails then loop through and increment by 1 each time
				for (i = 0; i < result.length; i++) {
					/**
					 * if the result from the array returns as not null print it out this is done
					 * because I initialised the array with 20 so rather than 10 printing correct
					 * and 10 printing "null" it will only print the ones that I want
					 */
					if (result[i] != null) {
						System.out.println(result[i]);
						System.out.println(" ");
					}
				}
				// Again I don't want the info to be lost in the menu
				System.out.print("Press any key and hit enter to continue: ");
				String anyKey = sc.next();
			}

			// 3. search for an employee by name
			else if (option == 3) {
				// ask the user to input employee's last name
				System.out.print("Please enter employee's last name: ");
				// take the user input and store in a string
				String last_name = sc.next();
				System.out.println(" ");
				// pass the stored string through the employee constructor in
				// getEmployeeByLastName
				Employee out = MajorAssignment.getEmployeeByLastName(dbc2.getConnection(), last_name);
				// print out the result to user
				System.out.println(out);
				System.out.println(" ");
				System.out.print("Press any key and hit enter to continue: ");
				String anyKey = sc.next();
			}

			/**
			 * 4. adding an employee on this option I wanted to add an extra feature that
			 * lets the user know if the employee number they have input is already in use
			 */
			else if (option == 4) {
				// create an called noEmp to begin while loop
				int noEmp = -1;
				// while the int is -1 loop through the options below
				while (noEmp == -1) {
					Scanner s = new Scanner(System.in);
					System.out.print("Please enter employee number: ");
					int empNo = s.nextInt();
					/**
					 * pass the employee number through a new procedure checkEmployeeExists which
					 * takes this empNo and passes it through a COUNT and if there is none present
					 * in the table it will result in 0 so continue on with the options
					 */
					int result = MajorAssignment.checkEmployeeExists(dbc2.getConnection(), empNo);
					if (result == 0) {
						System.out.print("Please enter employee birth date (YYYY-MM-DD): ");
						String bDate = sc.next();
						System.out.print("Please enter employee first name: ");
						String fname = sc.next();
						System.out.print("Please enter employee last name: ");
						String lname = sc.next();
						System.out.print("Please enter employee gender M or F: ");
						String gen = sc.next();
						System.out.print("Please enter employee hire date: ");
						String hDate = sc.next();
						System.out.print("Please enter employee salary: ");
						int salary = sc.nextInt();
						System.out.println(" ");
						// store this received data in an Employee but dont output yet
						Employee out = MajorAssignment.insertEmployee(dbc2.getConnection(), empNo, bDate, fname, lname,
								gen, hDate);
						/**
						 * Now I wanted another additional functionality by asking the user to input a
						 * salary for the new Employee, which will then insert into the oop_salaries
						 * table This was probably done wrong with the xresult variable however it
						 * seemed to work for me
						 */
						int xresult = MajorAssignment.insertSalary(dbc2.getConnection(), empNo, salary);
						/**
						 * Now we can call back the employee that we stored earlier and append it onto
						 * the salary that we took which is also added to the table which we can then
						 * view in option 5 and 6
						 */
						System.out.println(out + " Salary: " + salary);
						// let user know employee & salary has been added
						System.out.println("Employee + Salary added");
						System.out.println(" ");
						System.out.print("Press any key and hit enter to continue: ");
						String anyKey = sc.next();
						break;
					}
					/**
					 * if the result returns 1 i.e the SQL finds a number already in the table then
					 * tell the user it already exists and loop back around to ask the user again
					 */
					else {
						System.out.println("Employee Number already exists");
					}

				}
			}
			/**
			 * 5. This option will use 2 tables, oop_employees and oop_salaries and display
			 * the employee along with their salary beside it
			 */
			else if (option == 5) {
				// this is another array done similar to option 2
				Employee[] result = MajorAssignment.getAllDetailsAndSalaries(dbc2.getConnection());
				int i;
				for (i = 0; i < result.length; i++) {
					// again we have set the length below to 20 so every result that returns
					// a null value will not be printed out
					if (result[i] != null) {
						System.out.println(result[i]);
						System.out.println(" ");
					}
				}
				System.out.print("Press any key and hit enter to continue: ");
				String anyKey = sc.next();
			}
			// 6. Additional functionality
			else if (option == 6) {
				// an array similar to option 6
				Employee[] result = MajorAssignment.getEmployeeDetailsByDepartment(dbc2.getConnection());
				int i;
				for (i = 0; i < result.length; i++) {
					/*
					 * similar to option 5 above prints out if the result that is returned is not
					 * null
					 */
					if (result[i] != null) {
						System.out.println(result[i]);
						System.out.println(" ");
					}
				}
				System.out.print("Press any key and hit enter to continue: ");
				String anyKey = sc.next();
			}

			// unknown option
			else {
				System.out.println("Invalid option.");
			}
		} while (true); // loop until option 0 is selected

		// Close the connection
		dbc2.closeConnection();
		// Close the scanner
		sc.close();
		System.out.println("MajorAssignment.java - ending main method\n");

	}

	/**
	 * THE FOLLOWING METHODS CALL SQL PROCEDURES STORED IN YOUR MYSQL SCHEMA AND
	 * RETURNs THE RESULTS TO THE IF/ELSE BLOCKS ABOVE WHERE THEY WERE CALLED
	 * 
	 * Each method calls one or more stored SQL procedures from our MySQL schema
	 * There is currently have one SQL procedure defined in the provided
	 * procedures.sql file located in the data folder. You will need to - Create
	 * additional procedures in procedures.sql - Import them into your schema using
	 * Import.java - Write code in the if/else blocks above to call java methods
	 * below which in turn call procedures in your MySQL schema - All of these steps
	 * combined should enable you to have 7 options (0 to 6) on your user menu.
	 * Using option 1 as a template you must implement option 2 to 6.
	 * 
	 * The general format of these methods is as follows - give the method an
	 * appropriate access modifier, return type, name & parameters - Create a string
	 * containing an SQL statement which calls a procedure - Create a statement -
	 * Create a results set by executing your statement on the MySQL schema - Return
	 * the results set as a java primitive or object - Close the statement - Close
	 * the results set
	 */

	/**
	 * Option 0 - Exit the program. No method to call SQL required.
	 */

	/**
	 * Option 1 - Display the count of the employees
	 * 
	 * @param conn - the connection to the MySQL schema
	 * @return count of employees
	 */
	public static int getEmployeeCount(Connection conn) {
		// initialise an int to begin try statement
		int num = -1;
		try {
			// Create the SQL query string which uses the "getEmployeeCount" stored
			// procedure in the employee
			String sql = "CALL getEmployeeCount()";
			// Create a new SQL statement
			Statement st = conn.createStatement();
			// Create a new results set which store the value returned by the when the sql
			// statement is executed
			ResultSet rs = st.executeQuery(sql);
			// While there are still elements in the results set
			while (rs.next())
				num = rs.getInt(1); // assign the next int in the results set to num
			rs.close(); // close the results set
			st.close(); // close the statement
		} catch (SQLException e) {
			System.out.println("Error in getEmployeeCount");
			e.printStackTrace();
		}
		// returns the value of num after the try statement has sent the SQL statement
		// over
		return num;
	}

	/**
	 * Option 2 - Display records of all the employees
	 * 
	 * @param conn - the connection to the MySQL schema
	 */
	public static Employee[] getAllEmployeeDetails(Connection conn) {
		Employee[] empArray = new Employee[20];
		try {
			// Create the SQL query string which uses the "getEmployeeCount" stored
			// procedure in the employee
			String sql = "CALL getAllEmployeeDetails()";
			// Create a new SQL statement
			Statement st = conn.createStatement();
			// Create a new results set which store the value returned by the when the sql
			// statement is executed
			ResultSet rs = st.executeQuery(sql);
			// start off i at 0
			int i = 0;
			// loop through the result set from 0 taking in the variables
			while (rs.next())
				empArray[i++] = new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6));
			rs.close(); // close the results set
			st.close(); // close the statement
		} catch (SQLException e) {
			System.out.println("Error in getEmployeeCount");
			e.printStackTrace();
		}
		// return values which can be used in the main method above
		return empArray;
	}

	/**
	 * Option 3 - Search for an employee by last name
	 * 
	 * @param conn  - the connection to the MySQL schema
	 * @param lname - Last name provided
	 * @return
	 */
	public static Employee getEmployeeByLastName(Connection conn, String lname) {
		// create a new employee object
		Employee output = new Employee();
		try {
			// Create the SQL query string which uses the
			String sql = "CALL getEmployeeByLastName(\"" + lname + "\")";
			// Create a new SQL statement
			Statement st = conn.createStatement();
			// Create a new results set which store the value returned by the when the sql
			// statement is executed
			ResultSet rs = st.executeQuery(sql);
			// loop through the returned data set from the sql statement above storing
			// the values in the variables below
			while (rs.next())
				output = new Employee(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6));
			rs.close(); // close the results set
			st.close(); // close the statement
		} catch (SQLException e) {
			System.out.println("Error in getEmployeeByLastName");
			e.printStackTrace();
		}
		// return an employee which can be accessed in main method above
		return output;
	}

	/**
	 * Option 4 - Add a new employee. This method need only alter the Employee table
	 * 
	 * @param conn - the connection to the MySQL schema
	 * @param emp  - the employee to add
	 */
	public static Employee insertEmployee(Connection conn, int empNo, String bDate, String fname, String lname,
			String gen, String hDate) {
		// create a new employee object
		Employee output = new Employee();
		try {
			// sql statement to insert the values passed down from main method above
			String sql = "CALL insertEmployee(" + empNo + ",\"" + bDate + "\",\"" + fname + "\",\"" + lname + "\",\""
					+ gen + "\",\"" + hDate + "\")";
			// create a sql statement
			Statement st = conn.createStatement();
			// execute the sql statement
			st.executeUpdate(sql);
			// store this statement in a new employee object with the below parameters
			output = new Employee(empNo, bDate, fname, lname, gen, hDate);
		} catch (SQLException e) {
			System.out.println("Error in insertEmployee");
			e.printStackTrace();
		}
		// returns the new output which can be accessed in the main method
		return output;

	}

	/**
	 * Option 4.1 added feature to allow the user to input a salary for the new
	 * employee seperate SQL file to insert into another table
	 * 
	 * @param conn
	 * @param empNo
	 * @param salary
	 * 
	 */

	// parameters taken from main method input above
	public static int insertSalary(Connection conn, int empNo, int salary) {
		try {
			// calling a sql statement
			String sql = "CALL insertSalary(" + empNo + ",\"" + salary + "\")";
			// execute the statement to insert the values
			Statement st = conn.createStatement();
			st.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("Error in insertEmployee");
			e.printStackTrace();
		}
		/*
		 * i don't need this to return anything of importance from this method It will
		 * insert the salary info but return the amount that the user inputs themselves
		 */
		return -1;
	}

	/**
	 * Option 4.2 check if employee already exists added feature to check if an
	 * employee already exists through a simple COUNT script
	 * 
	 * @param conn
	 * @param empNo
	 * 
	 */
	// take the empNo input from main method above
	public static int checkEmployeeExists(Connection conn, int empNo) {
		// initialise an int to begin
		int num = -1;
		try {
			// Create the SQL query string which will do a COUNT
			// and return either 0 or 1 to determine if employee already exists
			String sql = "CALL checkEmployeeExists(\"" + empNo + "\")";
			// Create a new SQL statement
			Statement st = conn.createStatement();
			// Create a new results set which store the value returned when the sql
			// statement is executed
			ResultSet rs = st.executeQuery(sql);
			while (rs.next())
				// gets the value of the COUNT expression from above
				num = rs.getInt(1);
			rs.close(); // close the results set
			st.close(); // close the statement

		} catch (SQLException e) {
			System.out.println("Error in checkEmployeeExists");
			e.printStackTrace();
		}
		// returns the result of the COUNT expression which can be accessed in the main
		// method above
		return num;
	}

	/**
	 * Option 5 - Retrieve and display all employee details + salaries from salary
	 * table
	 * 
	 * @param conn - the connection to the MySQL schema
	 */
	public static Employee[] getAllDetailsAndSalaries(Connection conn) {
		/**
		 * creating an employee array with 20 values in it which is why I have if
		 * statements with != null in main method above
		 */
		Employee[] empArray = new Employee[20];
		try {
			// Create the SQL query string
			String sql = "CALL getAllDetailsAndSalaries";
			// Create a new SQL statement
			Statement st = conn.createStatement();
			// Create a new results set
			ResultSet rs = st.executeQuery(sql);
			// While there are still elements in the results set
			int i = 0;
			// begin the array at 0 and iterate through incrementing by 1 each time
			while (rs.next())
				empArray[i++] = new Employee(
						// take in these values below into an employee class
						rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getInt(7));

			rs.close(); // close the results set
			st.close(); // close the statement
		} catch (SQLException e) {
			System.out.println("Error in getAllDetailsAndSalaries");
			e.printStackTrace();
		}
		// return the value of the empArray made in the while loop above which
		// can be accessed in main method
		return empArray;

	}

	/**
	 * Option 6 - Retrieve and display all employee details + salaries + department
	 * and order by department number
	 * 
	 * @param conn - the connection to the MySQL schema
	 */

	public static Employee[] getEmployeeDetailsByDepartment(Connection conn) {
		// create another array with 20 values
		Employee[] empArray = new Employee[20];
		try {
			// Create the SQL query string
			String sql = "CALL getEmployeeDetailsByDepartment";
			// Create a new SQL statement
			Statement st = conn.createStatement();
			// Create a new results set
			ResultSet rs = st.executeQuery(sql);
			// initialise an int at 0
			int i = 0;
			while (rs.next())
				/**
				 * begin the array at 0 (i) and iterate through incrementing by 1 in the case of
				 * this output we are putting the details of the departments first as it is
				 * ordered by department so therefore the first two variables must match the
				 * results from the Department table!!!
				 */
				empArray[i++] = new Employee(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9));
			rs.close(); // close the results set
			st.close(); // close the statement
		} catch (SQLException e) {
			System.out.println("Error in getEmployeeDetailsByDepartment");
			e.printStackTrace();
		}
		// return the empArray which can be accessed in main method above
		return empArray;
	}

}