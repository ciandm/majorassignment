import java.sql.Date;

/**
 * FOLLOW THE COMMENTS CAREFULLY TO COMPLETE THIS FILE
 * 
 * Employee class containing fields which directly relate to column in the
 * Employees table in MySQL database
 * 
 * @author Alex Cronin
 *
 */
public class Employee {

	// private fields to store data
	private int emp_no;
	private String birth_date;
	private String first_name;
	private String last_name;
	private String gender;
	private String hire_date;
	private int salary;
	private int department_no;
	private String department_name;

	/**
	 * Constructor
	 * @param emp_no
	 * @param birth_date
	 * @param first_name
	 * @param last_name
	 * @param gender
	 * @param hire_date
	 */
	// provide the correct arguments to the constructor and assign the parameter
	// values to the appropriate instance variables
	public Employee() {
		super();

	}

	public Employee(String first_name, String last_name) {
		this.first_name = first_name;
		this.last_name = last_name;
	}

	/**
	 * CONSTRUCTOR FOR OPTION 2
	 * 
	 * @param emp_no
	 * @param birth_date
	 * @param first_name
	 * @param last_name
	 * @param gender
	 * @param hire_date
	 */
	public Employee(int emp_no, String birth_date, String first_name, String last_name, String gender,
			String hire_date) {
		super();
		this.emp_no = emp_no;
		this.birth_date = birth_date;
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.hire_date = hire_date;
		// set these as set values so if statements can be made later on
		this.salary = -1;
		this.department_no = -1;
		this.department_name = "";

	}

	/**
	 * CONSTRUCTOR FOR OPTION 5
	 * 
	 * @param emp_no
	 * @param birth_date
	 * @param first_name
	 * @param last_name
	 * @param gender
	 * @param hire_date
	 * @param salary
	 */

	public Employee(int emp_no, String birth_date, String first_name, String last_name, String gender, String hire_date,
			int salary) {
		super();
		this.emp_no = emp_no;
		this.birth_date = birth_date;
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.hire_date = hire_date;
		this.salary = salary;
		this.department_no = -1;
		this.department_name = "";
	}

	/**
	 * CONSTRUCTOR FOR OPTION 6
	 * 
	 * @param emp_no
	 * @param birth_date
	 * @param first_name
	 * @param last_name
	 * @param gender
	 * @param hire_date
	 * @param salary
	 * @param department_no
	 * @param department_name
	 */

	public Employee(int department_no, String department_name, int emp_no, String birth_date, String first_name,
			String last_name, String gender, String hire_date, int salary) {
		super();
		this.emp_no = emp_no;
		this.birth_date = birth_date;
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.hire_date = hire_date;
		this.salary = salary;
		this.department_no = department_no;
		this.department_name = department_name;
	}

	/**
	 * getter and setter methods
	 */

	// emp_no getter
	public int getEmp_no() {
		return emp_no;
	}

	// emp_no setter
	public void setEmp_no(int emp_no) {
		this.emp_no = emp_no;
	}

	// birth_date getter
	public String getBirth_date() {
		return birth_date;
	}

	// birth_date setter
	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	// first_name getter
	public String getFirst_name() {
		return first_name;
	}

	// first_name setter
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	// last_name getter
	public String getLast_name() {
		return last_name;
	}

	// last_name setter
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	// gender getter
	public String getGender() {
		return gender;
	}

	// gender setter
	public void setGender(String gender) {
		this.gender = gender;
	}

	// hire_date getter
	public String getHire_date() {
		return hire_date;
	}

	// hire_date setter
	public void setHire_date(String hire_date) {
		this.hire_date = hire_date;
	}
	/**
	 * to string method for employee object so it can be printed out from main
	 * method in MajorAssignment
	 */
	public String toString() {
		// initialise the string so it can be added to later
		String empString = "";
		/**
		 * from MajorAssignment option 6 I wanted to order by department I
		 * had department details on the left hand side I want it to check if any
		 * department values return and if yes take them and add it onto the values in
		 * the next series
		 * department_no is set as -1 above so if a value returns from sql statements then it 
		 * obviously will not be -1 so it will carry out line 205 - 207
		 */
		if (this.department_no != -1) {
			empString = empString + "Dept No: " + this.department_no;
			empString = empString + " Dept Name: " + this.department_name + " ";
		}
		/** 
		 * this is the main toString that will display for option 2 & 3
		 * if department_no is not over-ridden with a value i.e the user presses option 6
		 * then carry out the below toString 
		 */
		empString = empString + "Emp_no: " + this.emp_no + " Birth Date: " + this.birth_date + " First Name: "
				+ this.first_name + " Last Name: " + this.last_name + " Gender: " + this.gender + " Hire Date: "
				+ this.hire_date;
		/** like above the salary is set at -1 so if a value comes over from sql statement in MA
		 * then it will tag the salary onto the end of the string and if not it will skip adding
		 * the salary altogether
		 */
		if (this.salary != -1) {
			empString = empString + " Salary: " + this.salary;
		}

		return empString;

	}

}
